# importing pandas as pd
import pandas as pd
import os
import configparser
config = configparser.ConfigParser()  # создаём объекта парсера
config.read("set.ini")
to_excel = config["DIR"]["DirPathToExcel"]
to_csv = config["DIR"]["DirPathToCSV"]
try:
    while True:
        for filenames in os.listdir(rf"{to_excel}"):
            excel_file = pd.ExcelFile(rf"{to_excel}{filenames}")
            for sheet_names in excel_file.sheet_names:
                read_file = pd.read_excel(excel_file, sheet_name=sheet_names)
                read_file.to_csv(rf"{to_csv}{sheet_names}.csv", index=None, header=True)
                print(rf"CSV file already created in {to_csv}")
except Exception as f:
    print(f"error due to {f}")