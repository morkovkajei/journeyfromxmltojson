import time
import stomp
import os
from confluent_kafka import Producer
import socket
import xmltodict
import xml.etree.ElementTree as ET
import json
import datetime
import configparser  # импортируем библиотеку


config = configparser.ConfigParser()  # создаём объекта парсера
config.read("settings.ini")  # читаем конфиг
kafkaServer = config["KAFKA"]["server"]
amqQueue = config["ACTIVEMQ"]["queue"]
amqHost = config["ACTIVEMQ"]["host"]
amqPort = config["ACTIVEMQ"]["port"]


def takeCurrentTime():
    current_time = datetime.datetime.now().time()
    return current_time


### Connecting to ActiveMQ
hosts = [(amqHost, amqPort)]
conn = stomp.Connection(host_and_ports=hosts)
conn.connect('admin', 'admin', wait=True)
conn.subscribe(destination= amqQueue, id=1, ack='auto', headers={'transformation':'jms-map-xml'})
time.sleep(2)
print('Time: ' + str(takeCurrentTime()) + ' | Log: Подключился к серверу ActiveMQ')


### Conntcting to RedPanda
config = {
    'bootstrap.servers': kafkaServer,
}

producer = Producer(config)
print('Time: ' + str(takeCurrentTime())  + ' | Log: Подключился к серверу RedPanda')


def send(topic,message):
        producer.produce(topic, value=message)
        producer.flush()

class MsgListener(object):
    def on_message(self,message):
        print(f'Time: {str(takeCurrentTime())} | Log: {message.body}' )
        send(message.headers['topic'],transform(message.body))

### Converting message to dictionary
def transform(message):

    root = ET.fromstring(message)

    to_string = ET.tostring(root, encoding='UTF-8', method='xml')
    xml_to_dict = xmltodict.parse(to_string)
    return str(xml_to_dict)



while True:
    conn.set_listener('printer',MsgListener())
