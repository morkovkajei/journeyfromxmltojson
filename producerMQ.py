import stomp


hosts = [('127.0.0.1', 61616)]
conn = stomp.Connection(host_and_ports=hosts)
conn.connect('admin', 'admin', wait=True)
with open("nig.xml","r") as f:
    data = f.read()
conn.send(body=data, destination='/queue/test')