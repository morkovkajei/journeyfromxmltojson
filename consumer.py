import json
from kafka import KafkaConsumer


consumer = KafkaConsumer(
  bootstrap_servers=["localhost:19092"],
  group_id="demo-group",
  auto_offset_reset="earliest",
  enable_auto_commit=False,
  consumer_timeout_ms=1000,
)


consumer.subscribe("test")
for message in consumer:
    print(f'{message.offset}|{message.key} | {message.value}')